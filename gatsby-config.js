require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

const fsApi = require('netlify-cms-backend-fs/dist/fs/fs-express-api')

module.exports = {
  plugins: [
    'gatsby-plugin-netlify-cache',
    'gatsby-plugin-typescript',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
      {
        resolve: 'gatsby-transformer-bitbucket',
        options: {
          username: 'lukewestby',
          password: process.env.BITBUCKET_APP_PASSWORD,
        }
      },
    'gatsby-transformer-json',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'media',
        path: `${__dirname}/static/media`
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/content`,
        name: 'content',
      },
    },
    {
      resolve: 'gatsby-plugin-google-fonts',
      options: {
        fonts: [
          'Material Icons',
          'source sans pro:400,700'
        ],  
      },
    },
    {
      resolve: 'gatsby-plugin-netlify-cms',
      options: {
        modulePath: `${__dirname}/src/cms/start.js`,
        enableIdentityWidget: false,
        publicPath: 'admin',
        htmlTitle: 'Content Manager',
        manualInit: true,
      },
    },
    'gatsby-plugin-netlify'
  ],
  developMiddleware: fsApi
}
