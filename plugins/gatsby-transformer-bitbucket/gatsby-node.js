const Bitbucket = require('bitbucket')
const git = require('simple-git/promise')(process.cwd())
const path = require('path')

const bitbucket = new Bitbucket()

exports.onCreateNode = async (helpers, { username, password }) => {
  bitbucket.authenticate({
    type: 'basic',
    username,
    password,
  })
  const { node, actions, createNodeId } = helpers
  const { createNode, createParentChildLink } = actions
  if (node.internal.type !== 'File') return
  actions.touchNode({ nodeId: node.id })
  const filepath = path.relative(process.cwd(), node.absolutePath)
  const logs = await git.log({ file: filepath })
  const hash = logs.all[logs.total - 1].hash
  const commit = await bitbucket.commits.get({
    node: hash,
    repo_slug: 'website',
    username: 'sunrisemvmtsb',
  })
  const user = commit.data.author.user
  const childNode = {
    author: {
      name: user.display_name,
      avatar: user.links.avatar.href,
    },
    id: createNodeId(`${node.id} >> BitbucketFile`),
    children: [],
    parent: node.id,
    internal: {
      contentDigest: node.internal.contentDigest,
      type: 'BitbucketFile'
    }
  }
  createNode(childNode)
  createParentChildLink({ parent: node, child: childNode })
}