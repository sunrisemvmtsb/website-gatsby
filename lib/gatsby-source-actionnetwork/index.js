const fetch = require('node-fetch')

exports.sourceNodes = ({ token }) => async ({
  actions,
  schema,
  createNodeId,
  createContentDigest,
}) => {
  const { createNode, createTypes } = actions

  try {
    const result = await fetch('https://actionnetwork.org/api/v2/events', {
      headers: {
        'OSDI-API-Token': token,
      },
    }).then(r => r.json())
    
    const events = result._embedded['osdi:events']

    events.forEach((event) => {
      const record = {
        id: createNodeId(event.identifiers[0]),
        location: event.location,
        title: event.title,
        description: event.description,
        created: event.created_date,
        start: event.start_date,
        visibility: event.visibility,
        status: event.status,
        url: event.browser_url,
        instructions: event.instructions,
        hidden: event['action_network:hidden']
      }

      const node = Object.assign(record, {
        parent: null,
        children: [],
        internal: {
          type: 'ActionNetworkEvent',
          contentDigest: createContentDigest(record),
        },
      })

      createNode(node)
    })
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}