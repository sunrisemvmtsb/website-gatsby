require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

require('./lib/flat')
const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')
const remark = require('remark')
const remarkHtml = require('remark-html')
const cmsCollections = require('./src/cms/config')
const actionnetwork = require('./lib/gatsby-source-actionnetwork')

exports.sourceNodes = actionnetwork.sourceNodes({ token: process.env.ACTION_NETWORK_TOKEN })

exports.createSchemaCustomization = (helpers) => {
  const types = cmsCollections
    .map((c) => collectionToObject(helpers, c))
    .flat()

  helpers.actions.createTypes(types)
}

const collectionToObject = (helpers, collection) => {
  const typeName = toPascalCase(collection.name) + 'Json'
  if (collection.files) {
    let allTypes = []
    const unionFileTypes = []
    for (let file of collection.files) {
      const currentFileTypes = resolveObject(helpers, file, ['Node'], toPascalCase(collection.name))
      unionFileTypes.push(currentFileTypes[currentFileTypes.length - 1])
      allTypes = allTypes.concat(currentFileTypes)
    }
    allTypes.push(helpers.schema.buildObjectType({
      name: typeName,
      fields: Object.assign({
        pagepath: {
          type: 'String!',
          resolve: (source) =>
            createFilePath({ node: source, getNode: helpers.getNode })
              .replace('/pages', '')
        }
      }, ...unionFileTypes.map(t => t.config.fields)),
      interfaces: ['Node'],
      extensions: {
        infer: false
      }
    }))
    return allTypes
  } else {
    const types = resolveObject(helpers, collection, [ 'Node' ], toPascalCase(collection.name))
    const lastType = types[types.length - 1]
    types[types.length - 1] = helpers.schema.buildObjectType({
      name: typeName,
      fields: Object.assign({
        pagepath: {
          type: 'String!',
          resolve: (source) => createFilePath({ node: source, getNode: helpers.getNode })
        }
      }, lastType.config.fields),
      interfaces: ['Node'],
      extensions: {
        infer: false
      }
    })
    return types
  }
}

const resolveObject = (helpers, object, interfaces, baseName) => {
  const objectFields = {}
  let objectTypes = []
  for (let field of object.fields) {
    if (field.widget === 'hidden') {
      objectFields[field.name] = toPascalCase(typeof field.default) + '!'
    } else if (field.widget === 'string' || field.widget === 'text') {
      objectFields[field.name] = field.required ? 'String!' : 'String'
    } else if (field.widget === 'markdown') {
      const name = field.name
      objectFields[field.name] = {
        type: field.required ? 'String!' : 'String',
        resolve: (source) => {
          return typeof source[name] === 'string' ?
            remark()
              .use(remarkHtml)
              .processSync(source[name])
              .toString() :
            null
        }
      }
    } else if (field.widget === 'date') {
      objectFields[field.name] = field.required ? 'Date!' : 'Date'
    } else if (field.widget === 'object') {
      const nestedTypes = resolveObject(helpers, field, [], baseName + toPascalCase(object.name))
      const typeName = baseName + toPascalCase(object.name) + toPascalCase(field.name)
      objectFields[field.name] = field.required ? typeName + '!' : typeName
      objectTypes = objectTypes.concat(nestedTypes)
    } else if (field.widget === 'relation') {
      const collectionName = toPascalCase(field.collection) + 'Json'
      const name = field.name
      const valueField = field.valueField
      objectFields[field.name] = {
        type: field.required ? collectionName + '!' : collectionName,
        resolve: (source, args, context, info) => {
          return typeof source[name] !== 'undefined' && source[name] !== null ?
            context.nodeModel
              .getAllNodes({ type: collectionName })
              .find(n => n[valueField] === source[name]) :
            null
        },
      }
    } else if (field.widget === 'image') {
      const name = field.name
      objectFields[field.name] = {
        type: field.required ? 'ImageSharp!' : 'ImageSharp',
        resolve: (source, args, context, info) => {
          return typeof source[name] === 'string' ?
            context.nodeModel
              .getAllNodes({ type: 'ImageSharp' })
              .find(n => helpers.getNode(n.parent).absolutePath.endsWith(source[name])) :
            null
        }
      }
    } else if (field.widget === 'list') {
      if (field.fields) {
        const nestedTypes = resolveObject(helpers, field, [], baseName + toPascalCase(object.name))
        objectFields[field.name] = '[' + baseName + toPascalCase(object.name) + toPascalCase(field.name) + '!]!'
        objectTypes = objectTypes.concat(nestedTypes)
      } else {
        objectFields[field.name] = '[String!]!'
      }
    } else {
      throw Error('not implemented')
    }
  }

  objectTypes.push(helpers.schema.buildObjectType({
    name: baseName + toPascalCase(object.name),
    fields: objectFields,
    interfaces,
    extensions: {
      infer: false
    }
  }))
  return objectTypes
}

const toPascalCase = (input) => {
  return input[0].toUpperCase() + input.slice(1)
}

exports.createPages = async (helpers) => {
  const { actions, graphql, getNode, getNodesByType } = helpers
  const { createPage } = actions
  
  const allPageNodes = [
    getNodesByType('PagesJson'),
    getNodesByType('NewsJson')
  ]

  allPageNodes
    .flat()
    .filter((node) => node.template)
    .forEach((node) => {
      createPage({
        path: createFilePath({ node, getNode }).replace('/pages', ''),
        component: path.resolve(`src/templates/${node.template}.tsx`),
        context: {
          id: node.id,
          pagepath: createFilePath({ node, getNode }).replace('/pages', ''),
        },
      })
    })
}