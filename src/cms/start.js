import React from 'react'
import CMS, { init,  } from 'netlify-cms-app'
import FileSystemBackend from 'netlify-cms-backend-fs'
import HomePreview from './previews/index'
import collections from './config'
import * as Preview from '../contexts/Preview'

const makePreview = (Template) => ({ entry, getAsset, ...arg }) => {
  const data = entry.getIn(['data']).toJS()
  return data ?
    <Preview.Provider value={true}>
      <Template data={data} />
    </Preview.Provider> :
    <div>Loading...</div>
}

CMS.registerPreviewTemplate('home', makePreview(HomePreview))

CMS.registerPreviewStyle('https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Material+Icons')

CMS.registerBackend('file-system', FileSystemBackend)

init({
  config: {
    load_config_file: false,
    media_folder: 'static/media',
    public_folder: '/media',
    backend: window.location.hostname === 'localhost' ? {
      name: 'file-system',
      api_root: 'http://localhost:8000/api',
    } : {
      name: 'bitbucket',
      repo: 'sunrisemvmtsb/website',
      branch: 'main',
    },
    collections
  }
})