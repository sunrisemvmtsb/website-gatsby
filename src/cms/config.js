const makeImage = ({ name, label, required = true }) => ({
  name,
  label,
  widget: 'object',
  fields: [
    {
      name: 'file',
      label: 'File',
      widget: 'image',
      required: true,
    },
    {
      name: 'alt',
      label: 'Description',
      widget: 'string',
      required: true,
    },
  ]
})

module.exports = [
  {
    name: 'pages',
    label: 'Pages',
    files: [
      {
        name: 'home',
        label: 'Home',
        file: 'content/pages/index.json',
        format: 'json',
        fields: [
          {
            name: 'template',
            label: 'Template',
            widget: 'hidden',
            default: 'index'
          },
          {
            name: 'hero',
            label: 'Top Section',
            widget: 'object',
            fields: [
              makeImage({
                name: 'background',
                label: 'Background',
              }),
              {
                name: 'tagline',
                label: 'Tagline',
                widget: 'string',
                required: true
              }
            ]
          },
          {
            name: 'cta',
            label: 'CTA Section',
            widget: 'object',
            fields: [
              {
                label: 'Title',
                name: 'title',
                widget: 'string',
                required: true,
              },
              {
                label: 'Subtitle',
                name: 'subtitle',
                widget: 'string',
                required: false,
              },
              {
                label: 'Content',
                name: 'content',
                widget: 'markdown',
                required: true,
              },
              {
                label: 'Action',
                name: 'action',
                widget: 'string',
              },
              makeImage({
                label: 'Background',
                name: 'background',
              }),
            ],
          },
          {
            name: 'events',
            label: 'Events Section',
            widget: 'object',
            fields: [
              makeImage({
                name: 'background',
                label: 'Background',
              })
            ]
          }
        ]
      } 
    ]
  },
  {
    name: 'news',
    label: 'News',
    folder: 'content/news',
    format: 'json',
    create: true,
    slug: '{{slug}}-{{year}}-{{month}}-{{day}}',
    fields: [
      {
        name: 'template',
        label: 'Template',
        widget: 'hidden',
        default: 'news-entry',
      },
      {
        name: 'title',
        label: 'Title',
        widget: 'string',
        required: true,
      },
      {
        name: 'tags',
        label: 'Tags',
        widget: 'list',
      },
      makeImage({
        name: 'image',
        label: 'Image',
      }),
      {
        name: 'publishDate',
        label: 'Publish Date',
        widget: 'date',
        required: true,
      },
      {
        name: 'summary',
        label: 'Summary',
        widget: 'text',
        required: true,
      },
      {
        name: 'content',
        label: 'Content',
        widget: 'markdown',
        required: true,
      }
    ]
  }
]
