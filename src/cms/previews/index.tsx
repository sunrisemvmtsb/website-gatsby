import React from 'react'
import faker from 'faker'
import { IndexTemplate, UpdateData, EventData } from '../../templates/index'

const fakeUpdates: Array<UpdateData> = [0, 1, 2].map((s) => ({
  id: (faker.seed(s), faker.random.uuid()),
  title: faker.lorem.words(),
  publishDate: faker.date.recent().toISOString(),
  summary: faker.lorem.paragraph(),
  tags: [
    'Climate Crisis',
    'Direct Action',
    'Environmental Justice'
  ],
  image: {
    file: `https://picsum.photos/seed/${s * 100}/1024/768`,
    alt: faker.lorem.sentence(),
  },
  author: {
    name: faker.name.findName(),
  },
}))

const fakeEvents: Array<EventData> = [0, 1, 2].map((s) => ({
  id: (faker.seed(s), faker.random.uuid()),
  title: faker.lorem.words(),
  status: 'confirmed',
  url: 'https://actionnetwork.org/events/morbi-aliquet-nunc-non',
  visibility: 'visible',
  start: faker.date.future().toISOString(),
  location: {
    venue: faker.lorem.words(),
    region: faker.address.stateAbbr(),
    postal_code: faker.address.zipCode(),
    location: {
      latitude: parseFloat(faker.address.latitude()),
      longitude: parseFloat(faker.address.longitude()),
    },
    locality: faker.address.city(),
    country: 'USA',
    address_lines: [ faker.address.streetAddress() ],
  },
  description: [0, 1].map(() => `<p>${faker.lorem.paragraph()}</p>`).join(''),
  hidden: false,
  instructions: `<p>${faker.lorem.paragraph(3)}</p>`,
}))

export default ({ data }: any) => {
  const actualData = {
    hero: data.hero,
    updates: fakeUpdates,
    cta: data.cta,
    events: {
      background: data.events.background,
      entries: fakeEvents,
    }
  }
  return <IndexTemplate data={actualData} />
}