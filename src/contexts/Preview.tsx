import React from 'react'

const Context = React.createContext(false)

export const Provider = Context.Provider

export const usePreview = () => {
  return React.useContext(Context)
}