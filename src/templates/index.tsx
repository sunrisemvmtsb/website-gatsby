import React from 'react'
import { graphql } from 'gatsby'
import { FluidObject } from 'gatsby-image'
import cx from 'classnames'
import Styles from './index.module.css'
import Layout from '../components/Layout'
import Image from '../components/Image'
import Text from '../components/Text'
import Logo from '../components/Logo'
import Button from '../components/Button'
import Markdown from '../components/Markdown'
import * as Pallete from '../components/Pallete'
import Event from './index/Event'
import Update from './index/Update'
import SignupForm from './index/Signup'
import Instagram from './index/Instagram'

export type Data = {
  hero: HeroData,
  cta: CtaData,
  updates: Array<UpdateData>,
  events: EventsData,
}

export type ImageData = {
  file: FluidObject | string,
  alt: string,
}

export type HeroData = {
  tagline: string,
  background: ImageData,
}

export type CtaData = {
  title: string,
  subtitle: string | null,
  content: string,
  action: string,
  background: ImageData,
}

export type UpdateData = {
  id: string,
  title: string,
  publishDate: string,
  summary: string,
  tags: Array<string>,
  image: ImageData,
  author: {
    name: string,
  },
}

export type EventsData = {
  background: ImageData,
  entries: Array<EventData>
}

export type EventData = {
  title: string,
  status: string,
  url: string,
  visibility: string,
  start: string,
  location: {
    venue: string,
    region: string,
    postal_code: string,
    location: {
      latitude: number,
      longitude: number,
    },
    locality: string,
    country: string,
    address_lines: Array<string>
  },
  description: string,
  hidden: boolean
  id: string,
  instructions: string,
}

export const IndexTemplate = ({ data }: { data: Data }) => {
  return (
    <Layout>
      <Hero hero={data.hero} />
      <Signup />
      <Cta cta={data.cta} />
      {data.updates.length ? <Updates updates={data.updates} /> : null}
      {data.events.entries.length ? <Events background={data.events.background} entries={data.events.entries} /> : null}
      <Follow />
    </Layout>
  )
}

const Hero = ({ hero }: { hero: HeroData }) => {
  return (
    <section className={Styles.heroSection}>
      <Image
        style={{ position: 'absolute' }}
        className={Styles.heroBackgroundImage}
        src={hero.background.file} />
      <div className={Styles.heroBackgroundOverlay} />
      <div className={Styles.heroContent}>
        <div className={Styles.heroLogo}>
          <Logo
            height={132}
            color={Pallete.primary(Pallete.Alpha.Full)} />
        </div>
        <div>
          <Text
            color="on-overlay"
            emphasis="high"
            style="h4">
            We are Sunrise
          </Text>
        </div>
        <div className={Styles.heroTitle}>
          <Text
            color="on-overlay"
            emphasis="high"
            style="h2">
            Santa Barbara
          </Text>
        </div>
        <div className={Styles.heroTagline}>
          <Text
            color="on-overlay"
            emphasis="high"
            style="subtitle1">
            {hero.tagline}
          </Text>
        </div>
      </div>
    </section>
  )
}

const Signup = () => {
  return (
    <section className={Styles.signupSection}>
      <div className={Styles.signupContent}>
        <div className={Styles.signupTitle}>
          <Text
            color="on-surface"
            emphasis="high"
            style="h4">
            Join the Movement
          </Text>
        </div>
        <div className={Styles.signupSubtitle}>
          <Text
            color="on-surface"
            emphasis="medium"
            style="body2">
            Sign up to recieve regular email updates about what we are doing to bring climate justice to Santa Barbara, and how you can help. We respect your privacy.
          </Text>
        </div>
        <div>
          <SignupForm />
        </div>
      </div>
    </section>
  )
}

const Cta = ({
  cta
}: {
  cta: CtaData,
}) => {
  return (
    <section className={Styles.ctaSection}>
      <Image
        style={{ position: 'absolute' }}
        className={Styles.ctaBackgroundImage}
        src={cta.background.file} />
      <div className={Styles.ctaBackgroundOverlay} />
      <div className={Styles.ctaContent}>
        <div>
          <Text
            color="on-primary"
            style="h4"
            emphasis="high"
            paragraph>
            {cta.title}
          </Text>
          {cta.subtitle ?
            <Text
              color="on-primary"
              emphasis="medium"
              style="subtitle2"
              paragraph>
              {cta.subtitle}
            </Text> :
            null
          }
          <Markdown content={cta.content} />
        </div>
        <div></div>
      </div>
    </section>
  )
}

const Updates = ({
  updates,
}: {
  updates: Array<UpdateData>
}) => {
  return (
    <section className={Styles.updatesSection}>
      <div className={Styles.updatesContent}>
        <div className={Styles.updatesHeader}>
          <Text
            color="on-surface"
            emphasis="high"
            style="h4">
            Recent updates
          </Text>
        </div>
        <div className={Styles.updatesList}>
          {updates.map((u, i) => (
            <Update
              author={u.author.name}
              flipped={i % 2 === 1}
              image={u.image}
              publishDate={u.publishDate}
              summary={u.summary}
              tags={u.tags}
              title={u.title}
              key={u.id}
              />
          ))}
        </div>
        <div className={Styles.updatesViewAll}>
          <Button label="View all" />
        </div>
      </div>
    </section>
  )
}

const Events = ({
  background,
  entries,
}: {
  background: ImageData,
  entries: Array<EventData>,
}) => {
  return (
    <section className={Styles.eventsSection}>
      <Image
        style={{ position: 'absolute' }}
        className={Styles.eventsBackgroundImage}
        src={background.file} />
      <div className={Styles.eventsBackgroundOverlay} />
      <div className={Styles.eventsContent}>
        <div className={Styles.eventsHeader}>
          <Text
            color="on-overlay"
            emphasis="high"
            style="h4">
            Upcoming Events
          </Text>
        </div>
        <div className={Styles.eventsList}>
          {entries
            .filter((e) => Date.parse(e.start) >= Date.now())
            .sort((l, r) => -(Date.parse(l.start) - Date.parse(r.start)))
            .map((e) => (
              <Event
                key={e.id}
                latitude={e.location.location.latitude}
                longitude={e.location.location.longitude}
                address={`${e.location.address_lines.join(' ')}, ${e.location.locality}, ${e.location.region} ${e.location.postal_code}`}
                venue={e.location.venue}
                title={e.title}
                start={e.start}
                description={e.description}
                url={e.url} />
            ))}
        </div>
        <div className={Styles.eventsViewAll}>
          <Button
            href="https://calendar.google.com"
            target="_blank"
            label="Open Calendar"
            color="on-overlay" />
        </div>
      </div>
    </section>
  )
}

const Follow = () => {
  return (
    <section className={Styles.followSection}>
      <div className={Styles.followContent}>
        <div className={Styles.followTitle}>
          <Text
            style="h4"
            color="on-surface"
            emphasis="high">
            Follow us
          </Text>
        </div>
        <div>
          <Instagram />
        </div>
        <div className={Styles.followSignup}>

        </div>
      </div>
    </section>
  )
}


export const pageQuery = graphql`
  query IndexTemplate($pagepath: String!) {
    pagesJson(pagepath: { eq: $pagepath }) {
      hero {
        tagline
        background {
          alt
          file {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      cta {
        title
        subtitle
        content
        action
        background {
          alt
          file {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
      events {
        background {
          alt
          file {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    allActionNetworkEvent {
      nodes {
        title
        status
        url
        visibility
        start
        location {
          venue
          region
          postal_code
          location {
            latitude
            longitude
          }
          locality
          country
          address_lines
        }
        description
        hidden
        id
        instructions
      }
    }
    allNewsJson(limit: 3, sort: {fields: publishDate}) {
      nodes {
        id
        summary
        title
        publishDate
        tags
        parent {
          ...on File {
            childBitbucketFile {
              author {
                name
                avatar
              }
            }
          }
        }
        image {
          alt
          file{
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`

type Query = {
  pagesJson: {
    hero: {
      tagline: string,
      background: {
        alt: string,
        file: {
          fluid: FluidObject
        }
      },
    },
    cta: {
      title: string,
      subtitle: string,
      content: string,
      action: string,
      background: {
        alt: string,
        file: {
          fluid: FluidObject,
        },
      },
    },
    events: {
      background: {
        alt: string,
        file: {
          fluid: FluidObject
        },
      },
    },
  },
  allActionNetworkEvent: {
    nodes: Array<{
      title: string,
      status: string,
      url: string,
      visibility: string,
      start: string,
      location: {
        venue: string,
        region: string,
        postal_code: string,
        location: {
          latitude: number,
          longitude: number,
        },
        locality: string,
        country: string,
        address_lines: Array<string>
      },
      description: string,
      hidden: boolean
      id: string,
      instructions: string,
    }>,
  },
  allNewsJson: {
    nodes: Array<{
      id: string,
      summary: string,
      title: string,
      publishDate: string,
      tags: Array<string>,
      parent: {
        childBitbucketFile: {
          author: {
            name: string,
            avatar: string,
          },
        },
      },
      image: {
        alt: string,
        file: {
          fluid: FluidObject,
        },
      },
    }>,
  },
}

const transformQuery = (query: Query): Data => {
  return {
    hero: {
      tagline: query.pagesJson.hero.tagline,
      background: {
        file: query.pagesJson.hero.background.file.fluid,
        alt: query.pagesJson.hero.background.alt,
      }
    },
    cta: {
      title: query.pagesJson.cta.title,
      subtitle: query.pagesJson.cta.subtitle || null,
      action: query.pagesJson.cta.action,
      content: query.pagesJson.cta.content,
      background: {
        alt: query.pagesJson.cta.background.alt,
        file: query.pagesJson.cta.background.file.fluid,
      },
    },
    events: {
      background: {
        alt: query.pagesJson.events.background.alt,
        file: query.pagesJson.events.background.file.fluid,
      },
      entries: query.allActionNetworkEvent.nodes,
    },
    updates: query.allNewsJson.nodes.map((node) => {
      return {
        id: node.id,
        title: node.title,
        tags: node.tags,
        publishDate: node.publishDate,
        summary: node.summary,
        image: {
          alt: node.image.alt,
          file: node.image.file.fluid,
        },
        author: {
          name: node.parent.childBitbucketFile.author.name,
        },
      }
    })
  }
}

export default ({ data }: any) => {
  return <IndexTemplate data={transformQuery(data)} />
}