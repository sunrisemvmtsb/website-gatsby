import React from 'react'
import { graphql } from 'gatsby'
import Styles from './news-entry.module.css'
import Layout from '../components/Layout'

type Data = {
  title: string,
  summary: string,
  publishDate: string,
  content: string,
}

export const NewsEntryTemplate = ({
  data
}: {
  data: Data
}) => {
  return (
    <Layout>
      <div>
        <p>{data.title}</p>
        <p>{data.summary}</p>
      </div>
      <div>
        {data.content}
      </div>
    </Layout>
  )
}


export const pageQuery = graphql`
query NewsEntryTemplate($pagepath: String!) {
  newsJson(pagepath: {eq: $pagepath }) {
    title
    summary
    publishDate
    content
  }
}
`

type Query = {
  newsJson: {
    title: string,
    summary: string,
    publishDate: string,
    content: string,
  }
}

const transformQuery = (query: Query): Data => {
  return query.newsJson
}

export default({ data }: any) => {
  return <NewsEntryTemplate data={transformQuery(data)} />
}