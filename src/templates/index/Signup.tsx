import React from 'react'
import Styles from './Signup.module.css'

export default () => {
  const container = React.createRef<HTMLDivElement>()

  React.useLayoutEffect(() => {
    if (!container.current) return
    const script = document.createElement('script')
    script.async = true
    script.src = 'https://actionnetwork.org/widgets/v3/form/join-the-movement-91?format=js&source=widget&referrer=group-sunrise-movement-santa-barbara'
    const inner = document.createElement('div')
    inner.id = 'can-form-area-join-the-movement-91'
    container.current.append(script, inner)
  }, [container.current])

  return (
    <div className={Styles.container} ref={container}></div>
  )
}