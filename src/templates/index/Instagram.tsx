import React from 'react'

const fetchInstagramProfile = async () => {
  const response = await fetch('https://www.instagram.com/sunrisemvmtsb/')
  const html = await response.text()
  const firstHalf = html.split('window._sharedData = ')[1]
  const contents = firstHalf.split(';</script>')[0]
  const rawData = JSON.parse(contents)
  return rawData.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.map((edge: any) => {
    return {
      id: edge.node.id,
      src: edge.node.thumbnail_src,
      caption: edge.node.edge_media_to_caption.edges[0].node.text,
    }
  })
}

export default () => {
  const [entries, setEntries] = React.useState([])
  React.useEffect(() => {
    fetchInstagramProfile().then(setEntries)
  })

  return (
    <div>
      <div>
        {entries.map((e: any) => {
          return (
            <img key={e.id} src={e.src} alt={e.caption} />
          )
        })}
      </div>
    </div>
  )
}