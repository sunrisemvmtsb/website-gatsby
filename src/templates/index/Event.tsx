import React from 'react'
import Styles from './Event.module.css'
import Text from '../../components/Text'
import Map from '../../components/Map'
import Icon from '../../components/Icon'
import Button from '../../components/Button'
import * as Preview from '../../contexts/Preview'

export default ({
  latitude,
  longitude,
  venue,
  address,
  title,
  start,
  description,
  url,
}: {
  latitude: number,
  longitude: number,
  venue: string,
  address: string,
  title: string,
  start: string,
  description: string,
  url: string,
}) => {
  const startDate = new Date(start)
  const dateFormatter = new Intl.DateTimeFormat('en-iso', {
    dateStyle: 'long',
  })
  const timeFormatter = new Intl.DateTimeFormat('en-iso', {
    timeStyle: 'short',
  })
  const isPreview = Preview.usePreview()
  return (
    <div className={Styles.event}>
      <Map
        link="https://google.com"
        latitude={latitude}
        longitude={longitude} />
      <div className={Styles.eventDetails}>
        <div className={Styles.eventTitle}>
          <Text
            color="on-overlay"
            emphasis="high"
            style="h5"
            blocked={isPreview}>
            {title}
          </Text>
        </div>
        <Icon
          icon="place"
          color="on-overlay"
          emphasis="medium" />
        <div>
          <Text
            color="on-overlay"
            emphasis="medium"
            style="body1"
            paragraph
            blocked={isPreview}>
            {venue}
          </Text>
          <Text
            color="on-overlay"
            emphasis="medium"
            style="body1"
            paragraph
            blocked={isPreview}>
            {address}
          </Text>
        </div>
        <Icon
          icon="event"
          color="on-overlay"
          emphasis="medium" />
        <div>
          <Text
            color="on-overlay"
            emphasis="medium"
            style="body1"
            paragraph
            blocked={isPreview}>
            {dateFormatter.format(startDate)}
          </Text>
          <Text
            color="on-overlay"
            emphasis="medium"
            style="body1"
            paragraph
            blocked={isPreview}>
            {timeFormatter.format(startDate)}
          </Text>
        </div>
        <Icon
          icon="info"
          color="on-overlay"
          emphasis="medium" />
        <div>
          <Text
            color="on-overlay"
            emphasis="medium"
            style="body1"
            raw
            blocked={isPreview}>
            {description}
          </Text>
        </div>
        <div className={Styles.eventButton}>
          <Button
            href={url}
            color="on-overlay"
            target="_blank"
            label="RSVP" />
        </div>
      </div>
    </div>
  )
}