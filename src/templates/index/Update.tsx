import React from 'react'
import cx from 'classnames'
import { FluidObject } from 'gatsby-image'
import Styles from './Update.module.css'
import Image from '../../components/Image'
import Text from '../../components/Text'
import Button from '../../components/Button'
import * as Preview from '../../contexts/Preview'

export default ({
  flipped,
  image,
  title,
  author,
  publishDate,
  summary,
  tags,
}: {
  flipped: boolean,
  publishDate: string,
  title: string,
  author: string,
  summary: string,
  tags: Array<string>,
  image: {
    file: FluidObject | string,
  },
}) => {
  const date = new Date(publishDate)
  const formatter = new Intl.DateTimeFormat('en-iso', {
    dateStyle: 'long'
  })

  return (
    <article
      className={cx({
        [Styles.update]: true,
        [Styles.updateFlipped]: flipped
      })}>
      <Image
        src={image.file}
        className={Styles.updateImage} />
      <div className={Styles.updateDetails}>
        <div>
          <Text
            color="on-surface"
            emphasis="high"
            style="h5">
            {title}
          </Text>
        </div>
        <div className={Styles.updateByline}>
          <Text
            color="on-surface"
            emphasis="medium"
            style="caption">
            {formatter.format(date)} • {author}
          </Text>
        </div>
        <div className={Styles.updateSummary}>
          <Text
            color="on-surface"
            emphasis="medium"
            style="body2">
            {summary}
          </Text>
        </div>
        <div className={Styles.updateCategoriesHeader}>
          <Text
            color="on-surface"
            emphasis="low"
            style="overline">
            Categories
          </Text>
        </div>
        <div className={Styles.updateCategories}>
          {tags.map((t) => (
            <div
              key={t}
              className={Styles.updateCategory}>
              <Text
                color="on-surface"
                emphasis="medium"
                style="body2">
                {t}
              </Text>
            </div>
          ))}
        </div>
        <div>
          <Button label="Read More" />
        </div>
      </div>
    </article>
  )
}