import React from 'react'
import Text, { Color, Emphasis, Style } from './Text'
import Styles from './Paragraph.module.css'

export default (props: {
  color: Color,
  emphasis: Emphasis,
  style: Style,
  children: string | string[]
}) => {
  return (
    <p className={Styles.paragraph}>
      <Text {...props} /> 
    </p>
  )
}