import React from 'react'
import Styles from './Layout.module.css'

export default ({ children }: React.PropsWithChildren<{}>) => {
  return <div className={Styles.container}>{children}</div>
}