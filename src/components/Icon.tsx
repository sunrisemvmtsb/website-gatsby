import React from 'react'
import { Color, Emphasis } from './Text'

export type Icon =
  | 'event'
  | 'place'
  | 'info'
  | 'launch'

export default ({
  icon,
  color,
  emphasis,
}: {
  icon: Icon,
  color: Color,
  emphasis: Emphasis,
}) => {
  return (
    <span
      className="material-icons"
      style={{ color: `var(--color-${color}-${emphasis}-emphasis)` }}>
      {icon}
    </span>
  )
}