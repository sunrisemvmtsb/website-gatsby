import React from 'react'
import * as Preview from '../contexts/Preview'

const parseAsync = async (text: string): Promise<string> => {
  const { default: remark } = await import('remark')
  const { default: remarkHtml } = await import('remark-html')
  return new Promise((resolve, reject) => {
    remark()
      .use(remarkHtml)
      .process(text, (error, result) => {
        if (error) reject(error)
        else resolve(result!.toString())
      })
  })
}

const useMarkdown = (text: string): string | null => {
  const isPreview = Preview.usePreview()
  const [parsed, setParsed] = React.useState<string | null>(null)
  React.useEffect(() => {
    if (!isPreview) return
    let cancelled = false
    parseAsync(text)
      .then((output) => {
        if (!cancelled) setParsed(output)
      })
    return () => { cancelled = true }
  }, [text])
  return isPreview ? parsed : text
}

export default ({
  content
}: {
  content: string
}) => {
  const parsed = useMarkdown(content)

  return parsed ?
    <div dangerouslySetInnerHTML={{ __html: parsed }} /> :
    null
}