export enum Alpha {
  Full,
  BackgroundImageOverlay,
  HighEmphasis,
  MediumEmphasis,
  LowEmphasis,
  RestingBorder,
}

const rgba = (r,g,b,a) => {
  return `rgba(${r}, ${g}, ${b}, ${opacity(a)})`
}

export const primary = (a: Alpha): string => {
  return rgba(255,233,99,a)
}

export const surface = (a: Alpha): string => {
  return rgba(245,245,245,a)
}

export const overlay = (a: Alpha): string => {
  return rgba(18,18,18,a)
}

export const onOverlay = (a: Alpha): string => {
  return rgba(255,255,255,a)
}

export const onSurface = (a: Alpha) : string => {
  return rgba(0,0,0,a)
}

export const onPrimary = (a: Alpha): string => {
  return rgba(0,0,0,a)
}

export const opacity = (alpha: Alpha): number => {
  switch (alpha) {
    case Alpha.Full: return 1
    case Alpha.BackgroundImageOverlay: return 0.7
    case Alpha.HighEmphasis: return 0.87
    case Alpha.MediumEmphasis: return 0.6
    case Alpha.LowEmphasis: return 0.38
    case Alpha.RestingBorder: return 0.12
  }
}
