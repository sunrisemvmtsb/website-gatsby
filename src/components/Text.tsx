import React, { CSSProperties } from 'react'
import cx from 'classnames'
import Styles from './Text.module.css'

export type Style =
  | 'h2'
  | 'h4'
  | 'h5'
  | 'h6'
  | 'subtitle1'
  | 'subtitle2'
  | 'body1'
  | 'body2'
  | 'button'
  | 'caption'
  | 'overline'

export type Color =
  | 'on-surface'
  | 'on-overlay'
  | 'on-primary'

export type Emphasis =
  | 'high'
  | 'medium'
  | 'low'

const collapseChildren = (input: string | string[]| undefined): string => {
  if (Array.isArray(input)) return input.join('')
  else if (typeof input === 'string') return input
  return ''
}

export default ({
  style = 'body1',
  color = 'on-surface',
  emphasis = 'high',
  raw = false,
  paragraph = false,
  children,
  blocked,
}: {
  style: Style,
  color: Color,
  emphasis: Emphasis,
  paragraph?: boolean,
  raw?: boolean,
  blocked?: boolean,
  children?: string | string[],
}) => {
  const [parsed, setParsed] = React.useState<string | null>(null)
  React.useLayoutEffect(() => {
    if (!raw) return
    const parser = new DOMParser()
    const doc = parser.parseFromString(collapseChildren(children), 'text/html')
    const paragraphs = Array.from(doc.querySelectorAll('p'))
    const div = doc.createElement('div')
    div.append(...paragraphs)
    setParsed(div.innerHTML)
  }, [raw, children])

  if (raw) {
    return parsed === null ?
      null :
      <div
        className={cx({
          [Styles.text]: true,
          [Styles.raw]: true,
          [Styles[style]]: true,
          [Styles.blocked]: blocked,
        })}
        style={{
          color: `var(--color-${color})`,
          opacity: `var(--opacity-${emphasis}-emphasis)`,
        }}
        dangerouslySetInnerHTML={{ __html: parsed }} />
  }

  return paragraph ?
    <p
      className={cx({
        [Styles.text]: true,
        [Styles.paragraph]: true,
        [Styles[style]]: true,
        [Styles.blocked]: blocked,
      })}
      style={{
        color: `var(--color-${color})`,
        opacity: `var(--opacity-${emphasis}-emphasis)`,
      }}
      children={children} /> :
    <span
      className={cx({
        [Styles.text]: true,
        [Styles[style]]: true,
        [Styles.blocked]: blocked,
      })}
      style={{
        color: `var(--color-${color})`,
        opacity: `var(--opacity-${emphasis}-emphasis)`,
      }}
      children={children} />
}