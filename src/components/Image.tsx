import React from 'react'
import Img, { FluidObject } from 'gatsby-image'

export default ({
  src,
  style,
  className
}: {
  src: FluidObject | string,
  style?: React.CSSProperties,
  className?: string
}) => {
  const [aspectRatio, setAspectRatio] = React.useState<number | null>(null)
  const [height, setHeight] = React.useState<number | null>(null)
  React.useEffect(() => {
    if (typeof src !== 'string') return
    const img = new Image()
    img.src = src
    img.decode().then(() => {
      setHeight(img.naturalHeight)
      setAspectRatio(img.naturalWidth / img.naturalHeight)
    })
    return () => { img.src = '' }
  }, [src])

  if (typeof src === 'string') {
    return aspectRatio !== null ?
      <Img
        fluid={{
          src: src,
          srcSet: '',
          aspectRatio,
          sizes: `(max-width: ${height}px) 100vw, ${height}px`
        }}
        style={style}
        className={className}/> :
        null
  }

  return (
    <Img
      fluid={src}
      style={style}
      className={className} />
  )
}