import React from 'react'
import Styles from './Button.module.css'
import Text, { Color } from './Text'

export default ({
  label,
  color = 'on-surface',
  href,
  target,
}: {
  label: string,
  color?: Color,
  href?: string,
  target?: string,
}) => {
  if (href) {
    return (
      <a
        href={href}
        target={target}
        className={Styles.button}
        style={{ borderColor: `var(--color-${color}-resting-border)` }}>
        <Text
          color={color}
          emphasis="medium"
          style="button">
          {label}
        </Text>
      </a>
    )
  }

  return (
    <button
      className={Styles.button}
      style={{ borderColor: `var(--color-${color}-resting-border)` }}>
      <Text
        color={color}
        emphasis="medium"
        style="button">
        {label}
      </Text>
    </button>
  )
}